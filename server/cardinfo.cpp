#include "cardinfo.h"
CardInfo::CardInfo(QString PAN_)
{
    QSqlDatabase data = QSqlDatabase::addDatabase("QSQLITE");
    data.setDatabaseName("server_base.db");
    QSqlQuery query;
    bool state = query.exec(QString("SELECT * FROM cards WHERE PAN = '%1'").arg(PAN_));
    if(!state){
        throw(query.lastError().text());
    }

    QSqlRecord record = query.record();
    query.next();
    this->PAN = PAN_;
    this->PIN = query.value(record.indexOf("PIN")).toString();
    this->ballance = query.value(record.indexOf("ballance")).toDouble();
    this->name = query.value(record.indexOf("name")).toString();
    this->expiryDate = query.value(record.indexOf("expiryDate")).toString();
    data.close();
}


double CardInfo::getBallance(){
    return this->ballance;
}

void CardInfo::setBallance(double ballance){
    this->ballance = ballance;
}

void CardInfo::debugPrint(){
    qDebug() << "PAN:" << this->PAN;
    qDebug() << "PIN:" << this->PIN;
    qDebug() << "ballance" << this->ballance;
    qDebug() << "name:" << this->name;
    qDebug() << "expiryDate:" << this->expiryDate;
}

bool CardInfo::compare(Transaction &tr){
    bool name = !(this->name.compare(tr.getName()));
    bool date = !(this->expiryDate.compare(tr.getExpiryDate()));
    bool PIN = !(this->PIN.compare(tr.getPIN()));
    return name&&date;
}

void CardInfo::update(){
    QSqlDatabase data = QSqlDatabase::addDatabase("QSQLITE");
    data.setDatabaseName("server_base.db");
    QSqlQuery query;
    QString request = QString("UPDATE cards SET ballance = '%1' WHERE PAN = '%2'").arg(this->ballance).arg(this->PAN);
    bool state = query.exec(request);
    if(!state){
        throw(query.lastError().text());
    }

}
