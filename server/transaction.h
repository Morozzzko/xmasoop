#ifndef TRANSACTION_H
#define TRANSACTION_H

#include "card.h"
#include "QtSql"
#include "QDate"

class Transaction : public Card
{
protected:
    qint32 transactionCode;
    qint32 response;
    double amount;
    QString PIN;
    QDate transactionDate;
public:
    QString idReturn;
    Transaction(Card &card, QString PIN);
    Transaction();
    Transaction(QString PAN, QString name, QString expiryDate, qint32 code, QString PIN, double amount);

    QString getPIN() const;

    bool setResponse(qint32 resp);
    qint32 getResponse() const;
    QString getResponseText() const;

    bool setCode(qint32 code);
    qint32 getCode() const;

    void setAmount(double amount);
    double getAmount() const;

    QDate getTransactionDate() const;
    void  setTransactionDate(QDate date);

    //DB
    void log(bool server);
    bool findBadTransaction();
    void truncateLocalBase();
    //QStringList findLast();

    //Serialise
    friend QDataStream& operator<<(QDataStream&, Transaction&);
    friend QDataStream& operator>>(QDataStream&, Transaction&);


    QString toString() {
        QString result;
        result += QString::number(getCode()) + " - " + getPIN() + " - " + QString::number(getResponse()) + " - " + getPAN() + " - " + getExpiryDate() + " - " + getName();
        return result;
    }

};

#endif // TRANSACTION_H
