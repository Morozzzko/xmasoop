#ifndef NETWORKOPERATOR_H
#define NETWORKOPERATOR_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include "transaction.h"
class NetworkOperator : public QObject
{
    Q_OBJECT
private:
    QTcpServer server;
    QTcpSocket* connection;

    int port;
public:
    explicit NetworkOperator(int port, QObject *parent = 0);
    ~NetworkOperator();
    void send(QList<Transaction>&);
    void send(Transaction&);
    void sendBallance(double value);

protected:
    void listen();

signals:

public slots:
    void packetReceived();
    void acceptConnection();
    void sessionTerminated();
};

#endif // NETWORKOPERATOR_H
