#include "card.h"

Card::Card(QString PAN_, QString name_, QString expiryDate_)
{
    this->PAN = PAN_;
    this->name = name_;
    this->expiryDate = expiryDate_;
}

Card::Card(){
    this->PAN = "";
    this->name = "";
    this->expiryDate = "";
}

QString Card::getName(){
    return this->name;
}

QString Card::getPAN(){
    return this->PAN;
}

QString Card::getExpiryDate(){
    return this->expiryDate;
}
