#include "networkoperator.h"

#include "transaction.h"
#include "cardinfo.h"
#include <QDataStream>
#include <QTcpSocket>
#include <iostream>
using namespace std;

NetworkOperator::NetworkOperator(int port, QObject *parent) : QObject(parent)
{
    this->port = port;

    listen();
    connect(&(this->server), SIGNAL(newConnection()), this, SLOT(acceptConnection()));
}

NetworkOperator::~NetworkOperator()
{

}

void NetworkOperator::acceptConnection() {

    connection = server.nextPendingConnection();

    cout << "Incoming connection from " << connection->peerAddress().toString().toStdString() << ":" << connection->peerPort() << ".";

    connect(connection, SIGNAL(disconnected()),
                this, SLOT(sessionTerminated()));
    connect(connection, SIGNAL(disconnected()),
                connection, SLOT(deleteLater()));
    connect(connection, SIGNAL(readyRead()),
                this, SLOT(packetReceived()));
    cout << "The server won't accept any connections until current session is terminated" << endl;

    server.close();
}

void NetworkOperator::packetReceived() {
    QDataStream in(connection);
    qint8 count;
    in >> count;
    if (count == -1) {
        cout << "Received ballance request" << endl;
        try {
            Transaction tr;
            in >> tr;
            CardInfo card(tr.getPAN());
            sendBallance(card.getBallance());
        }
        catch (QString err) {
            qDebug() << err;
        }

        connection->readAll();
        return;
    }

    cout << "Received packet of " << count << " transactions." << endl;
    for (int i = 0; i < count; i++) {
        Transaction data;
        in >> data;
        cout << data.toString().toStdString() << endl;
        // deal with packets here
        try{
        CardInfo info(data.getPAN());
            if (info.compare(data)){
                    data.log(true);
                    switch (data.getCode())
                    {
                    case 1: {
                        double newBallance = info.getBallance()-data.getAmount();
                        if (newBallance >= 0){
                            info.setBallance(newBallance);
                            info.update();
                            data.setResponse(0);
                        } else {
                            data.setResponse(1);
                        }
                        //send
                        break;
                    }

                    case 2: {
                        double newBallance = info.getBallance()+data.getAmount();
                        info.setBallance(newBallance);
                        info.update();
                        data.setResponse(0);
                        //send
                        break;
                    }
                    }

            } else {
                data.setResponse(3);
                //send
            }
        }
        catch(QString str){
            //send
            qDebug() << str;
        }
    }
    connection->readAll();
}

void NetworkOperator::sessionTerminated() {
    listen();
}

void NetworkOperator::listen() {
    cout << "Starting a server at localhost:" << port << "... ";
    if (!(server.listen(QHostAddress::LocalHost, port))) {
        cout << "Failed!";
        return;
    }
    cout << "Success!" << endl;
    cout << "Listening to incoming connections... " << endl;
}
void NetworkOperator::send(QList<Transaction> & src) {
    QDataStream out(connection);
    out << (qint8)src.size();
    for (QList<Transaction>::iterator i = src.begin(); i != src.end(); i++) {
        out << *i;
    }
}
void NetworkOperator::send(Transaction & src) {
    QDataStream out(connection);
    out << (qint8)1;
    out << src;
}

void NetworkOperator::sendBallance(double value) {
    QDataStream out(connection);
    out << (qint8)(-1);
    out << value;
}


