#ifndef CARDINFO_H
#define CARDINFO_H

#include "QString"
#include "QtSql"
#include "QDebug"
#include "transaction.h"
class CardInfo
{
private:
    QString PAN;
    QString PIN;
    double ballance;
    QString name;
    QString expiryDate;
public:
    CardInfo(QString PAN_);
    bool compare(Transaction& tr);
    double getBallance();
    void setBallance(double ballance);
    void update();
    void debugPrint(); //FOR TESTING
};

#endif // CARDINFO_H
