#include "transaction.h"
#include <QDataStream>


Transaction::Transaction(Card& card, QString PIN)
{
    response = -1;
    this->PAN = card.getPAN();
    this->expiryDate = card.getExpiryDate();
    this->name = card.getName();
    this->PIN = PIN;
    this->response = -1;
    //this->amount = amount;
    this->transactionDate = QDate::currentDate();
    /*if ((code>=0)&&(code<3)){
        this->transactionCode = code;
    } else {
        throw(QString ("Error: invalid transaction code"));
    }*/
}

Transaction::Transaction(QString PAN, QString name, QString expiryDate, qint32 code, QString PIN, double amount)
{
    response = 0;
    this->PAN = PAN;
    this->name = name;
    this->expiryDate = expiryDate;
    this->PIN = PIN;
    this->response = -1;
    this->transactionDate = QDate::currentDate();
    this->amount = amount;
    if ((code>=0)&&(code<3)){
        this->transactionCode = code;
    } else {
        throw(QString ("Error: invalid transaction code"));
    }
}


Transaction::Transaction()
{
    response = 0;
    transactionCode = 0;
}

QString Transaction::getPIN() const{
    return this->PIN;
}

qint32 Transaction::getCode() const{
    return this->transactionCode;
}


qint32 Transaction::getResponse() const{
    return this->response;
}

QString Transaction::getResponseText() const{
    switch (this->response)
    {
    case 0:
        return QString("Операция выполнена успешно");
        break;
    case 1:
        return QString("Недостаточно средств");
        break;
    case 2:
        return QString("Истек срок транзакции");
        break;
    case 3:
        return QString("Неверный PIN");
        break;
    case 4:
        return QString("Неверный PAN");
        break;
    case 5:
        return QString("Карта просрочена");
        break;
    case 6:
        return QString("Превышин лимит попыток ввода PIN");
        break;
    default:
        return QString("Ответ не получен");
    }
}

bool Transaction::setResponse(qint32 resp){
    if ((resp>=0)&&(resp<7)){
        this->response = resp;
        return true;
    } else{
        return false;
    };
}

bool Transaction::setCode(qint32 code){
    if ((code>=0)&&(code<3)){
        this->transactionCode = code;
        return true;
    } else {
        return false;
    }
}

void Transaction::setAmount(double amount){
    this->amount = amount;
}

double Transaction::getAmount() const{
    return this->amount;
}

QDate Transaction::getTransactionDate() const{
    return this->transactionDate;
}

void Transaction::log(bool server){
    QSqlDatabase data = QSqlDatabase::addDatabase("QSQLITE");
    QString base_name = server ? "server_base.db" : "client_base.db";
    data.setDatabaseName(base_name);
    data.open();
    QSqlQuery query;
    QString request = QString("INSERT INTO transactions VALUES(NULL, '%1','%2','%3','%4','%5')").arg(this->PAN)
            .arg(this->transactionCode)
            .arg(QString::number(this->amount))
            .arg(this->transactionDate.toString())
            .arg(QString::number(this->response));
    qDebug() << request;
    bool state = query.exec(request);
    data.close();
    if (!state){
        qDebug() << query.lastError().text();
        throw (query.lastError().text());
    }
}

bool Transaction::findBadTransaction(){
    QSqlDatabase data = QSqlDatabase::addDatabase("QSQLITE");
    data.setDatabaseName("client_base.db");
    data.open();
    QSqlQuery query;
    QString request = QString("SELECT id FROM transactions WHERE response = '-1'");
    bool state = query.exec(request);
    if (!state){
        throw (query.lastError().text());
    }
    if (!query.isActive()){
        return false;
    } else {
        QSqlRecord rec = query.record();
        query.next();
        this->setCode(query.value(rec.indexOf("code")).toInt());
        this->setAmount(query.value(rec.indexOf("amount")).toDouble());
        this->setTransactionDate(query.value(rec.indexOf("date")).toDate());
        data.close();
        return true;
    }
}

void Transaction::truncateLocalBase(){
    QSqlDatabase data = QSqlDatabase::addDatabase("QSQLITE");
    data.setDatabaseName("client_base.db");
    data.open();
    QSqlQuery query;
    QString request = QString("TRUNCATE TABLE transactions");
    bool state = query.exec(request);
    if (!state){
        throw (query.lastError().text());
    }
    data.close();
}

/*
QStringList Transaction::findLast(){
    QStringList out;
    QSqlDatabase data = QSqlDatabase::addDatabase("QSQLITE");
    data.setDatabaseName("server_base.db");
    data.open();
    QSqlQuery query;
    QString request = QString("SELECT id FROM transactions LIMIT 10;");
    bool state = query.exec(request);
    if (!state){
        throw (query.lastError().text());
    }
    QSqlRecord rec = query.record();
    while(query.next()){
        out << query.value(0).toString();
    }
    return out;
}
*/
QDataStream& operator>>(QDataStream& src, Transaction& tr) {
    src >> tr.PAN;
    src >> tr.PIN;
    src >> tr.expiryDate;
    src >> tr.transactionCode;
    src >> tr.response;
    src >> tr.name;
    src >> tr.amount;
    src >> tr.transactionDate;
    src >> tr.response;
    src >> tr.idReturn;
    return src;
}

void  Transaction::setTransactionDate(QDate date){
    this->transactionDate = date;
}

QDataStream& operator<<(QDataStream& s, Transaction& tr) {
    return s << tr.getPIN() << tr.getPAN() << tr.getExpiryDate()
             << tr.getCode() << tr.getResponse() << tr.getName()
             << tr.getAmount() << tr.getTransactionDate() << tr.response << tr.idReturn;
}
