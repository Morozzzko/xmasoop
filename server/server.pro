#-------------------------------------------------
#
# Project created by QtCreator 2014-12-16T23:14:23
#
#-------------------------------------------------

QT       += core
QT       += sql
QT       -= gui


QT       += network

TARGET = server
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    networkoperator.cpp \
    card.cpp \
    cardinfo.cpp \
    transaction.cpp

HEADERS += \
    networkoperator.h \
    card.h \
    cardinfo.h \
    transaction.h
