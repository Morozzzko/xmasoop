#-------------------------------------------------
#
# Project created by QtCreator 2014-12-17T20:35:45
#
#-------------------------------------------------

QT += core gui network widgets sql

TARGET = client
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    card.cpp \
    transaction.cpp

HEADERS  += mainwindow.h \
    card.h \
    transaction.h

FORMS    += mainwindow.ui
