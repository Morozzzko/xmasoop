#ifndef CARD_H
#define CARD_H

#include "QString"

class Card
{
protected:
    QString PAN;
    QString name;
    QString expiryDate;
public:
    Card();
    Card(QString PAN_, QString name_, QString expiryDate_);
    QString getPAN();
    QString getName();
    QString getExpiryDate();
};

#endif // CARD_H
