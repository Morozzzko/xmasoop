#ifndef NETWORKOPERATOR_H
#define NETWORKOPERATOR_H

#include <QObject>
#include <QTcpSocket>
#include "transaction.h"

class NetworkOperator : public QObject
{
    Q_OBJECT
private:

public:
    explicit NetworkOperator(QObject *parent = 0);
    ~NetworkOperator();


signals:

public slots:
};

#endif // NETWORKOPERATOR_H
