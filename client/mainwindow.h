#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "networkoperator.h"
#include "transaction.h"
#include <QList>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void connectToServer(int port);

    void send(QList<Transaction>&);
    void send(Transaction&);

    void updateBallance();
    void requestBallance();
    
private slots:
    void on_payButton_clicked();

    void on_returnButton_clicked();

    void on_closeButton_clicked();

public slots:
    void packetReceived();
    void connected();
    void displayError(QAbstractSocket::SocketError socketError);


private:
    Ui::MainWindow *ui;
    Card card = Card("1111 1111 1111 1111","Vlad","0316");
    Transaction transaction;
    QTcpSocket socket;
    double ballance;
};

#endif // MAINWINDOW_H
