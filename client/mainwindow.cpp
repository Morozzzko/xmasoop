#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "transaction.h"

#include <QInputDialog>

#include <QHostAddress>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    int input = QInputDialog::getInt(this, tr("PIN"), tr("Enter ваш pin"), 0, 0000, 9999);

    QString PIN = QString("%1").arg(input, 4, 10, QChar('0'));
    transaction = Transaction(card,PIN);

    //ui->account->display(PIN);

    connectToServer(22228);

    connect(&socket, SIGNAL(connected()), this, SLOT(connected()));

    connect(&socket, SIGNAL(readyRead()), this, SLOT(packetReceived()));
    connect(&socket, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(displayError(QAbstractSocket::SocketError)));

    ballance = 0;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_payButton_clicked()
{
    double value = QInputDialog::getDouble(this, tr("Оплата товара"), tr("Сумма к оплате:"), 0, 0);
    transaction.setAmount(value);
    transaction.setCode(1);
    transaction.log(false);
    //this->ui->account->display(QString::number(value));
    send(transaction);

}

void MainWindow::on_returnButton_clicked()
{
    QStringList items;
    bool ok = true;
    for(int i = 0; i<10; i++){
        items << QString::number(i);
    }

    QString returnID = QInputDialog::getItem(this, tr("Выберите заказ для возврата"),
                                             tr("Заказы:"), items, 0, false, &ok);
    if(ok && !returnID.isEmpty()){
        transaction.idReturn = returnID;
        transaction.setCode(2);
        transaction.log(false);

        send(transaction);
        //serialise
    }
}

void MainWindow::on_closeButton_clicked()
{
    transaction.setCode(0);
    transaction.setAmount(0);
    while (transaction.findBadTransaction()){
        send(transaction);
        //serialise and send
    }
    transaction.truncateLocalBase();
}

void MainWindow::requestBallance() {
    QDataStream out(&socket);
    out << (qint8)(-1);
    out << transaction;
}

void MainWindow::connectToServer(int port) {
    socket.abort();
    socket.connectToHost(QHostAddress::LocalHost, port);
}


void MainWindow::connected() {
    requestBallance();
}

void MainWindow::packetReceived() {
    QDataStream sock(&socket);
    qint8 count;
    sock >> count;
    if (count == -1) {
        sock >> ballance;
        updateBallance();
    }
    else {
        for (int i = 0; i < count; i++) {
            Transaction data;
            sock >> data;
            // Do something with the data
        }
    }
    socket.readAll();
}

void MainWindow::updateBallance() {
    this->ui->account->display(ballance);
}

void MainWindow::send(QList<Transaction> & src) {
    QDataStream out(&socket);
    out << (qint8)src.size();
    for (QList<Transaction>::iterator i = src.begin(); i != src.end(); i++) {
        out << *i;
    }
}
void MainWindow::send(Transaction & src) {
    QDataStream out(&socket);
    out << (qint8)1;
    out << src;
}

void MainWindow::displayError(QAbstractSocket::SocketError socketError) {

}
