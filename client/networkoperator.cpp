#include "networkoperator.h"

#include <QMessageBox>
#include <QHostAddress>


NetworkOperator::NetworkOperator(QObject *parent) : QObject(parent)
{
}

NetworkOperator::~NetworkOperator()
{

}

void MainWindow::connectToServer(int port) {
    socket.abort();
    socket.connectToHost(QHostAddress::LocalHost, port);
}


void MainWindow::connected() {

}

void MainWindow::packetReceived() {
    QDataStream sock(&socket);
    quint8 count;
    sock >> count;
    for (int i = 0; i < count; i++) {
        Transaction data;
        sock >> data;
        // Do something with the data
    }
    socket.readAll();
}

void MainWindow::displayError(QAbstractSocket::SocketError socketError) {

}
